package ru.t1.sukhorukova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.data.DataXmlLoadFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data in json xml.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");

        getDomainEndpoint().xmlLoadFasterXmlData(new DataXmlLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
